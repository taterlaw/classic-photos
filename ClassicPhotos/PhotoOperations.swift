import Foundation
import UIKit

// This enum contains all the possible states a photo record can be in
enum PhotoRecordState {
    case new, downloaded, filtered, failed
}

class PhotoRecord {
    let name: String
    let url: URL
    var state: PhotoRecordState = .new
    var image = UIImage(named: "Placeholder")
    
    init(name:String, url:URL) {
        self.name = name
        self.url = url
    }
}

class PendingOperations {
    // 'lazy' means they are not initialized until they are accessed
    // Dictionary to keep track of downloads
    lazy var downloadsInProgress: [IndexPath: Operation] = [:]
    lazy var downloadQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Download Queue"
        // Max number of threads downloadQueue is allowed to spawn. Leaving this out will
        // allow the queue to decide how many threads needed on it own.
        //queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    // Dictionary to keep track of filter operations
    lazy var filtrationsInProgress: [IndexPath: Operation] = [:]
    lazy var filtrationQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Image Filtration Queue"
        // Max number of threads downloadQueue is allowed to spawn. Leaving this out will
        // allow the queue to decide how many threads needed on it own.
        //queue.maxConcurrentOperationCount = 1
        return queue
    }()
}

// Operation is an abstract class, designed for subclassing. Each subclass represents a
// specific task.
class ImageDownloader: Operation {
    // Add a constant reference to the PhotoRecord object related to the operation.
    let photoRecord: PhotoRecord
    
    // Create a designated initializer allowing the photo record to be passed in.
    init(_ photoRecord: PhotoRecord) {
        self.photoRecord = photoRecord
    }
    
    // main() is the method you override in Operation subclasses to actually perform work.
    override func main() {
        // Check for cancellation before starting. Operations should regularly check if
        // they have been cancelled before attempting long or intensive work.
        if isCancelled {
            return
        }
        
        // Download the image data.
        guard let imageData = try? Data(contentsOf: photoRecord.url) else { return }
        
        // Check again for cancellation.
        if isCancelled {
            return
        }
        
        // If there is data, create an image object and add it to the record, and move the
        // state along. If there is no data, mark the record as failed and set the
        // appropriate image.
        if !imageData.isEmpty {
            photoRecord.image = UIImage(data:imageData)
            photoRecord.state = .downloaded
        } else {
            photoRecord.state = .failed
            photoRecord.image = UIImage(named: "Failed")
        }
    }
}

class ImageFiltration: Operation {
    let photoRecord: PhotoRecord
    
    init(_ photoRecord: PhotoRecord) {
        self.photoRecord = photoRecord
    }
    
    // main() is the method you override in Operation subclasses to actually perform work.
    override func main () {
        if isCancelled {
            return
        }
        
        guard self.photoRecord.state == .downloaded else {
            return
        }
        
        if let image = photoRecord.image,
            let filteredImage = applySepiaFilter(image) {
            photoRecord.image = filteredImage
            photoRecord.state = .filtered
        }
    }
    
    func applySepiaFilter(_ image: UIImage) -> UIImage? {
        guard let data = UIImagePNGRepresentation(image) else { return nil }
        let inputImage = CIImage(data: data)
        
        if isCancelled {
            return nil
        }
        
        let context = CIContext(options: nil)
        
        guard let filter = CIFilter(name: "CISepiaTone") else { return nil }
        filter.setValue(inputImage, forKey: kCIInputImageKey)
        filter.setValue(0.8, forKey: "inputIntensity")
        
        if isCancelled {
            return nil
        }
        
        guard
            let outputImage = filter.outputImage,
            let outImage = context.createCGImage(outputImage, from: outputImage.extent)
            else {
                return nil
        }
        
        return UIImage(cgImage: outImage)
    }
}
