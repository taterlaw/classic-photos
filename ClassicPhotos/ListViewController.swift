/*
 http://bit.ly/2tmU4pO
 
 Task: a simple, single piece of work that needs to be done.
 Thread: a mechanism provided by the OS that allows multiple sets of instructions to perate at
    the same time within a single application.
 Process: an executable chuck of code, which can be made up of multiple threads.
 */

import UIKit
import CoreImage

// Database of picture details
let dataSourceURL
    = URL(string:"http://www.raywenderlich.com/downloads/ClassicPhotosDictionary.plist")!

class ListViewController: UITableViewController {
    // Data model for the view
    var photos: [PhotoRecord] = []
    // Manages the threaded tasks
    let pendingOperations = PendingOperations()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Classic Photos"
        fetchPhotoDetails()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath)
        
        // To provide feedback to the user, create a UIActivityIndicatorView and set it as
        // the cell’s accessory view.
        if cell.accessoryView == nil {
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            cell.accessoryView = indicator
        }
        let indicator = cell.accessoryView as! UIActivityIndicatorView
        
        // The data source contains instances of PhotoRecord. Fetch the correct one based
        // on the current indexPath.
        let photoDetails = photos[indexPath.row]
        
        // The cell’s text label is (nearly) always the same and the image is set
        // appropriately on the PhotoRecord as it is processed, so you can set them both
        // here, regardless of the state of the record.
        cell.textLabel?.text = photoDetails.name
        cell.imageView?.image = photoDetails.image
        
        // Inspect the record. Set up the activity indicator and text as appropriate,
        // and kick off the operations (not yet implemented).
        switch (photoDetails.state) {
        case .filtered:
            indicator.stopAnimating()
        case .failed:
            indicator.stopAnimating()
            cell.textLabel?.text = "Failed to load"
        case .new, .downloaded:
            indicator.startAnimating()
            // You tell the table view to start operations only if the table view is not
            // scrolling. These are actually properties of UIScrollView. Because
            // UITableView is a subclass of UIScrollView, table views automatically inherit
            // these properties.
            if !tableView.isDragging && !tableView.isDecelerating {
                startOperations(for: photoDetails, at: indexPath)
            }
        }
        
        return cell
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        // As soon as the user starts scrolling, you will want to suspend all operations
        // and take a look at what the user wants to see. You will implement
        // suspendAllOperations in just a moment.
        suspendAllOperations()
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // If the value of decelerate is false, that means the user stopped dragging the
        // table view. Therefore you want to resume suspended operations, cancel operations
        // for off-screen cells, and start operations for on-screen cells. You will
        // implement loadImagesForOnscreenCells and resumeAllOperations in a little
        // while, as well.
        if !decelerate {
            loadImagesForOnscreenCells()
            resumeAllOperations()
        }
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // This delegate method tells you that table view stopped scrolling, so you will
        // do the same as in #2.
        loadImagesForOnscreenCells()
        resumeAllOperations()
    }
    
    func suspendAllOperations() {
        pendingOperations.downloadQueue.isSuspended = true
        pendingOperations.filtrationQueue.isSuspended = true
    }
    
    func resumeAllOperations() {
        pendingOperations.downloadQueue.isSuspended = false
        pendingOperations.filtrationQueue.isSuspended = false
    }
    
    func loadImagesForOnscreenCells() {
        // Start with an array containing index paths of all the currently visible rows
        // in the table view.
        if let pathsArray = tableView.indexPathsForVisibleRows {
            // Construct a set of all pending operations by combining all the downloads
            // in progress and all the filters in progress.
            var allPendingOperations = Set(pendingOperations.downloadsInProgress.keys)
            allPendingOperations.formUnion(pendingOperations.filtrationsInProgress.keys)
            
            // Construct a set of all index paths with operations to be cancelled.
            // Start with all operations, and then remove the index paths of the
            // visible rows. This will leave the set of operations involving off-screen rows.
            var toBeCancelled = allPendingOperations
            let visiblePaths = Set(pathsArray)
            toBeCancelled.subtract(visiblePaths)
            
            // Construct a set of index paths that need their operations started.
            // Start with index paths all visible rows, and then remove the ones where
            // operations are already pending.
            var toBeStarted = visiblePaths
            toBeStarted.subtract(allPendingOperations)
            
            // Loop through those to be cancelled, cancel them, and remove their
            // reference from PendingOperations.
            for indexPath in toBeCancelled {
                if let pendingDownload = pendingOperations.downloadsInProgress[indexPath] {
                    pendingDownload.cancel()
                }
                pendingOperations.downloadsInProgress.removeValue(forKey: indexPath)
                if let pendingFiltration = pendingOperations.filtrationsInProgress[indexPath] {
                    pendingFiltration.cancel()
                }
                pendingOperations.filtrationsInProgress.removeValue(forKey: indexPath)
            }
            
            // Loop through those to be started, and call startOperations(for:at:) for each.
            for indexPath in toBeStarted {
                let recordToProcess = photos[indexPath.row]
                startOperations(for: recordToProcess, at: indexPath)
            }
        }
    }
    
    func startOperations(for photoRecord: PhotoRecord, at indexPath: IndexPath) {
        // Depending on the photo record’s state, you kick off either a download or
        // filter operation.
        switch (photoRecord.state) {
        case .new:
            startDownload(for: photoRecord, at: indexPath)
        case .downloaded:
            startFiltration(for: photoRecord, at: indexPath)
        default:
            // Logs an error message to the Apple System Log facility.
            NSLog("Do Nothing")
        }
    }
    
    func startDownload(for photoRecord: PhotoRecord, at indexPath: IndexPath) {
        // First, check for the particular indexPath to see if there is already an operation
        // in downloadsInProgress for it. If so, ignore this request.
        guard pendingOperations.downloadsInProgress[indexPath] == nil else {
            return
        }
        
        // If not, create an instance of ImageDownloader by using the designated initializer.
        let downloader = ImageDownloader(photoRecord)
        
        // Add a completion block which will be executed when the operation is completed.
        // This is a great place to let the rest of your app know that an operation has
        // finished. It’s important to note that the completion block is executed even
        // if the operation is cancelled, so you must check this property before doing
        // anything. You also have no guarantee of which thread the completion block is
        // called on, so you need to use GCD to trigger a reload of the table view on
        // the main thread.
        downloader.completionBlock = {
            if downloader.isCancelled {
                return
            }
            
            DispatchQueue.main.async {
                self.pendingOperations.downloadsInProgress.removeValue(forKey: indexPath)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }
        }
        
        // Add the operation to downloadsInProgress to help keep track of things.
        pendingOperations.downloadsInProgress[indexPath] = downloader
        
        // Add the operation to the download queue. This is how you actually get these
        // operations to start running — the queue takes care of the scheduling for you
        // once you’ve added the operation.
        pendingOperations.downloadQueue.addOperation(downloader)
    }
    
    func startFiltration(for photoRecord: PhotoRecord, at indexPath: IndexPath) {
        guard pendingOperations.filtrationsInProgress[indexPath] == nil else {
            return
        }
        
        let filterer = ImageFiltration(photoRecord)
        filterer.completionBlock = {
            if filterer.isCancelled {
                return
            }
            
            DispatchQueue.main.async {
                self.pendingOperations.filtrationsInProgress.removeValue(forKey: indexPath)
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }
        }
        
        pendingOperations.filtrationsInProgress[indexPath] = filterer
        pendingOperations.filtrationQueue.addOperation(filterer)
    }
    
    func fetchPhotoDetails() {
        let request = URLRequest(url: dataSourceURL)
        // Shows a spinning indicator in the status bar shows network activity.
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        // Create a URLSession data task to download the property list of images on a
        // background thread.
        let task = URLSession(configuration: .default).dataTask(with: request) { data, response, error in
            
            // Configure a UIAlertController to use in the event of an error.
            let alertController = UIAlertController(title: "Oops!",
                                                    message: "There was an error fetching photo details.",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default)
            alertController.addAction(okAction)
            
            if let data = data {
                do {
                    // If the request succeeds, create a dictionary from the property list.
                    // The dictionary uses the image name as the key and its URL as the value.
                    let datasourceDictionary =
                        try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as! [String: String]
                    
                    // Build the array of PhotoRecord objects from the dictionary.
                    for (name, value) in datasourceDictionary {
                        let url = URL(string: value)
                        if let url = url {
                            let photoRecord = PhotoRecord(name: name, url: url)
                            // Update the data model for the view
                            self.photos.append(photoRecord)
                        }
                    }
                    
                    // Build the array of PhotoRecord objects from the dictionary.
                    DispatchQueue.main.async {
                        // Turn off indication on status bar of network activity
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.tableView.reloadData()
                    }
                    // Return to the main thread to reload the table view and display the images.
                } catch {
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
            
            // Display the alert controller in the event of an error. Remember that
            // URLSession tasks run on background threads and display of any messages on
            // the screen must be done from the main thread.
            if error != nil {
                DispatchQueue.main.async {
                    // Turn off indication on status bar of network activity
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        // Run the download task.
        task.resume()
    }
}

